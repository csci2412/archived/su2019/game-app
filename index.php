<?php 
require_once('include/header.php');
$games = getGames();
?>
<main>
    <h1>Game Form Example</h1>
    <nav>
        <a href="game-form.php">Create new</a>
    </nav>
    <table>
        <tr>
            <th>Title</th>
            <th>Release Year</th>
            <th>System</th>
            <th>Developer</th>
            <th>Categories</th>
            <th>Completed</th>
            <th>Action</th>
        </tr>
        <?php foreach($games as $game): ?>
            <tr>
                <td><?=$game['title']?></td>
                <td><?=$game['release_year']?></td>
                <td><?=$game['system_name']?></td>
                <td><?=$game['developer_name']?></td>
                <td><?=$game['categories']?></td>
                <td><?=$game['completed'] ? 'Yes' : 'No'?></td>
                <td>
                    <a href="game-form.php?id=<?=$game['id']?>">Edit</a>
                    <a href="delete-game.php?id=<?=$game['id']?>"
                        onclick="return confirm('Are you sure?')">Delete</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</main>
<?php require_once('include/footer.php') ?>