<?php 
require_once 'include/header.php';
$game = getGame(); // return a game array or NULL
$devs = getDevelopers();
$systems = getSystems();
$categories = getCategories();
$msg = handleSubmit();
?>
<main>
    <nav>
        <a href="index.php">Game list</a>
    </nav>
    <?php if($msg):?>
        <p class="error"><?=$msg?></p>
    <?php endif;?>
    <form action="" method="post">
        <fieldset>
            <legend>Game info</legend>
            <input type="hidden" name="id"
                value="<?=$game ? $game->id : ''?>">
            <div>
                <label for="title">Game title</label>
                <input type="text" name="title" id="title"
                    value="<?=$game ? $game->title : ''?>">
            </div>
            <div>
                <label for="year">Release year</label>
                <input type="text" name="year" id="year"
                    value="<?=$game ? $game->release_year : ''?>">
            </div>
            <div>
                <label for="system">System</label>
                <select name="system" id="system">
                    <option></option>
                    <?php while($sys = $systems->fetch_object()):?>
                        <option value="<?=$sys->id?>"
                            <?=$game && $game->system_id === $sys->id 
                                ? 'selected' : ''?>><?=$sys->name?></option>
                    <?php endwhile;?>
                </select>
            </div>
            <div>
                <label for="developer">Developer</label>
                <select name="developer" id="developer">
                    <option></option>
                    <?php foreach($devs as $dev):?>
                        <option value="<?=$dev['id']?>"
                            <?=$game && $game->developer_id === $dev['id']
                                ? 'selected' : ''?>><?=$dev['name']?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div>
                <label for="categories">Categories</label>
                <select name="categories[]" id="categories" multiple>
                    <?php foreach($categories as $cat):?>
                        <option value="<?=$cat['id']?>"
                            <?=$game && in_array($cat['id'], $game->categories)
                                ? 'selected' : ''?>><?=$cat['name']?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div>
                <label>
                    <input type="checkbox" name="completed" id="completed"
                        <?=$game && $game->completed ? 'checked' : ''?>>Completed
                </label>
            </div>
            <div><button type="submit">Save game</button></div>
        </fieldset>
    </form>
</main>
<?php require_once 'include/footer.php' ?>