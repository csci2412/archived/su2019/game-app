<?php
require_once 'include/functions.php';

// null coalescing operator ??
$gameId = $_GET['id'] ?? null; // isset($_GET['id']) ? $_GET['id'] : null

if (!$gameId) {
    goToIndex();
}

$conn = new mysqli('localhost','root','','gamedb');
$query = "DELETE FROM game WHERE id = $gameId";
$result = $conn->query($query);

if ($result && $conn->affected_rows === 1) { 
    // we know we successfully deleted one game record
    goToIndex();
}
?>
Problem deleting game <a href="index.php">Go back</a>