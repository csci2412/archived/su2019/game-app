<?php require_once 'functions.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>game app</title>
    <style>
    body {
        font: normal 16px Arial, sans-serif;
    }
    nav {
        margin-bottom: 30px;
    }
    table {
        border-collapse: collapse;
    }
    td, th {
        border: 1px solid #ccc; /* light-gray border on cells */
        padding: 10px;
    }
    footer {
        margin-top: 60px;
    }
    fieldset {
        max-width: 300px;
    }
    fieldset > div {
        margin: 10px 0;
        display: flex;
        justify-content: space-between;
    }
    </style>
</head>
<body>
    
