<?php

function getConnection() {
    return new mysqli('localhost', 'root', '', 'gamedb');
}

function getResult(string $sql) {
    if (empty($sql)) {
        return null;
    }

    // new mysqli(host, username, password, database)
    $conn = getConnection();
    $result = $conn->query($sql); // either mysqli_result, true, or false

    if (!$result) {
        // something went wrong, deal with error somehow (error msg, log, etc)
        return null;
    }

    return $result;
}

function getGames() {
    $q = "SELECT g.id, g.title, g.release_year, 
            g.completed, s.name as system_name,
            d.name as developer_name,
            GROUP_CONCAT(c.name SEPARATOR ', ') as categories
        FROM game g
        JOIN system s ON s.id = g.system_id
        JOIN developer d ON d.id = g.developer_id
        LEFT JOIN game_category gc ON gc.game_id = g.id
        LEFT JOIN category c ON gc.category_id = c.id
        GROUP BY g.id
        ORDER BY g.title";

    return getResult($q);
}

function getGame() {
    if (!isset($_GET['id'])) {
        return null;
    }

    $id = $_GET['id'];
    $q = "select g.*,
            GROUP_CONCAT(gc.category_id SEPARATOR ', ') as categories
            from game g
            left join game_category gc ON g.id = gc.game_id
            where g.id = $id
            group by g.id";
    $result = getResult($q);

    if ($result && $result->num_rows === 1) {
        $game = $result->fetch_object(); // no need to loop because only one row
        $game->categories = explode(',', $game->categories);
        
        return $game;
    }

    return null;
}

function getSystems() {
    $q = 'SELECT * FROM system ORDER BY name';

    return getResult($q);
}

function getDevelopers() {
    $q = 'SELECT * FROM developer ORDER BY name';

    return getResult($q);
}

function getCategories() {
    $q = 'SELECT * FROM category ORDER BY name';

    return getResult($q);
}

function handleSubmit() {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        return null;
    }

    $gameId = $_POST['id']; // could be positive int or empty string
    $title = $_POST['title'];
    $year = $_POST['year'];
    $devId = $_POST['developer'];
    $sysId = $_POST['system'];
    $completed = isset($_POST['completed']) ? 1 : 0;
    $categories = $_POST['categories'] ?? [];
    $isNewGame = !isValidId($gameId);

    if (!isValidGame($title, $year, $devId, $sysId)) {
        return 'Please enter a valid title, year, developer, and system';
    }

    if ($isNewGame) {
        $query = "INSERT game(title, release_year, system_id, developer_id, completed)
            VALUES('$title', '$year', $sysId, $devId, $completed)";
    } else {
        $query = "UPDATE game SET title='$title', release_year='$year',
            system_id=$sysId, developer_id=$devId, completed=$completed
            WHERE id=$gameId";
    }

    $conn = getConnection();
    $result = $conn->query($query);
    
    if ($isNewGame) {
        $gameId = $conn->insert_id;
    }

    if (!$result) {
        return 'Problem saving game data';
    }

    // insert category records
    insertCategories($gameId, $categories, $isNewGame);

    goToIndex();
}

function insertCategories(string $gameId, array $categoryIds, bool $isNewGame) {
    if (!$isNewGame) {
        // delete existing game_category records
        getResult("DELETE FROM game_category WHERE game_id = $gameId");
    }

    if (count($categoryIds) === 0) {
        return;
    }

    // insert a new record for each category
    $insert = "INSERT game_category VALUES";
    $catRows = [];
    
    foreach($categoryIds as $catId) {
        array_push($catRows, "($gameId, $catId)");
    }
    $insert .= implode(',', $catRows);
    
    getResult($insert);
}

function goToIndex() {
    header('Location: index.php');
    exit();
}

function isValidId(string $id) : bool {
    return !empty($id) && is_numeric($id) && (int)$id > 0;
}

function isValidGame(string $title, string $year, string $devId, string $sysId) : bool {
    return !empty($title) && !empty($year) && !empty($devId) && !empty($sysId)
        && is_numeric($year);
}