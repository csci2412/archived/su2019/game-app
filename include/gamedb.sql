drop database if exists gamedb;
create database if not exists gamedb;
use gamedb;

create table `developer`(
	`id` int unsigned auto_increment primary key,
    `name` nvarchar(255) not null
);

create table `system`(
	`id` int unsigned auto_increment primary key,
    `name` nvarchar(255) not null
);

create table `category`(
	`id` int unsigned auto_increment primary key,
    `name` nvarchar(255) not null
);

create table `game`(
	`id` int unsigned auto_increment primary key,
    `title` nvarchar(255) not null,
    `completed` bool default false,
    `release_year` year not null,
    `system_id` int unsigned not null,
    `developer_id` int unsigned not null,
    foreign key (`system_id`) 
		references `system`(`id`) on delete cascade,
    foreign key (`developer_id`) 
		references `developer`(`id`)
);

create table `game_category`(
	`game_id` int unsigned,
    `category_id` int unsigned,
    primary key(`game_id`, `category_id`),
    foreign key(`game_id`)
		references `game`(`id`) on delete cascade,
	foreign key(`category_id`)
		references `category`(`id`) on delete cascade
);

insert into `system`(`name`) values('NES'), ('SNES'), ('PSX'), ('Dreamcast');
insert into `developer`(`name`) values('Nintendo'),('Capcom'),('Squaresoft'),
	('Team17');
insert into `category`(`name`) values('Platformer'),('Puzzle'),('RPG'),
	('Strategy');

#select * from system;
#select * from developer;    
#select * from category;

insert `game`(`title`,`release_year`,`completed`,`system_id`,`developer_id`)
values ('Super Mario Bros', '1985', true, 1, 1),
	('Mega Man', '1987', true, 1, 2),
	('Chrono Trigger', '1995', true, 2, 3);
    
insert into `game_category` values (1, 1), (2, 3);

select g.`title`, 
	g.`release_year`, 
    s.`name` as system,
    d.`name` as developer
from `game` g
join `system` s on g.system_id = s.id
join `developer` d on g.developer_id = d.id;

